var BFA = BFA || {};
BFA.Validator =  {};
(function (scope) {


    //Config
    //Options
    scope.options = {};

    //Init
    scope.init = function(){
        //
        scope.options.lang = (scope.options && scope.options.lang)? scope.options.lang : 'en';
        scope.options.errorView = app.Helper.defaultErrorView();

        //Popule rules
        app.Rule.List();
        app.Fields.getElements();
        app.Fields.Handlers();
    };


    //Fixes for IEs

    var ieFixes = function(){

        //getElementsByFixIE7
        // Needed to search for elements on ie7
        // I used this code from this awesome guy https://gist.github.com/eikes/2299607
        if (!document.getElementsByClassName) {
                document.getElementsByClassName = function(search) {
                    var d = document, elements, pattern, i, results = [];
                    if (d.querySelectorAll) { // IE8
                        return d.querySelectorAll("." + search);
                    }
                    if (d.evaluate) { // IE6, IE7
                        pattern = ".//*[contains(concat(' ', @class, ' '), ' " + search + " ')]";
                        elements = d.evaluate(pattern, d, null, 0, null);
                        while ((i = elements.iterateNext())) {
                            results.push(i);
                        }
                    } else {
                        elements = d.getElementsByTagName("*");
                        pattern = new RegExp("(^|\\s)" + search + "(\\s|$)");
                        for (i = 0; i < elements.length; i++) {
                        if ( pattern.test(elements[i].className) ) {
                            results.push(elements[i]);
                        }
                    }
                }
            return results;
          };
        }

        //BindIE7Fix
        //Another great soul http://stackoverflow.com/questions/11054511/how-to-handle-lack-of-javascript-object-bind-method-in-ie-8
        if (!Function.prototype.bind) {
          Function.prototype.bind = function(oThis) {
            if (typeof this !== 'function') {
              // closest thing possible to the ECMAScript 5
              // internal IsCallable function
              throw new TypeError('Function.prototype.bind - what is trying to be bound is not callable');
            }

            var aArgs   = Array.prototype.slice.call(arguments, 1),
                fToBind = this,
                fNOP    = function() {},
                fBound  = function() {
                  return fToBind.apply(this instanceof fNOP && oThis
                         ? this
                         : oThis,
                         aArgs.concat(Array.prototype.slice.call(arguments)));
                };

            fNOP.prototype = this.prototype;
            fBound.prototype = new fNOP();

            return fBound;
          };
        }

        //
        if ( ! window.console ) console = { log: function(){} };
    };



    // Ruler
    //// Init object
    var app = {};
    app.Rule = {};
    app.Rule.library = [];
    app.Rule.data = {};

    //// Ruler helpers

    ////// Get > get your rule
    app.Rule.get = function(rule){
        var library;
        for (var i = 0; i < app.Rule.library.length; i++) {
            library = app.Rule.library[i];
            if(rule === library['class']) return library;
        }
        return false;
    };

    app.Rule.setList = function(rule){
        var library;
        for (var i = 0; i < app.Rule.library.length; i++) {
            library = app.Rule.library[i];
            if(rule === library['class']) return library;
        }
        return false;
    };

    //// Ruler Methods

    ////// Validation > test and set default values
    app.Rule.Validation = function(newRule){
        // Validate name
        if(!newRule.name){
            return { 'error': true, 'message': 'Your rule needs a name'};
        }

        //Validate error message
        if(!newRule.errorMessage){
            newRule.errorMessage = {'en': 'Not a valid field'};
        }

        //Valid rule
        var validRegex;
        if(!newRule.validation){
            return { 'error': true, 'message': 'Your rule needs a regex'};
        }else{
            validRegex = true;
            try {
                newRule.validation  = new RegExp(newRule.validation);
            } catch(e) {
                return { 'error': true, 'message': 'Your rule needs a valid regex'};
            }
        }
        return true;
    };

    ////// List > add rules to the Validator (This can be a json file with all the rules available)
    app.Rule.listDefault = [
        //Not empty
        {
            'name': 'notEmpty',
            'class':'required',
            'errorMessage':{
                'pt': 'Erro: Campo obrigatorio',
                'en': 'Error: This field is required'
            },
            'validation': '^(?!\s*$).+'
        }
    ];

    app.Rule.List = function(){

        if(scope.options.addRules){
            //Todo validate fields
            for (var i = 0; i < scope.options.addRules.length; i++) {
                app.Rule.listDefault.push(scope.options.addRules[i]);
            }

        }

        //Add all
        for (var a = 0; a < app.Rule.listDefault.length; a++) {
            //Valid rule
            if( app.Rule.Validation(app.Rule.listDefault[a]) ){
                //Todo test with associative array, think might be easier to manipulate
                app.Rule.library.push(app.Rule.listDefault[a]);
            }
        }
    };



    // Fields
    app.Fields = [];
    app.Fields.library = [];
    app.Fields.forms = [];

    //// Fields helpers

    //// Field helpers > Get All elements that has the data attribute
    app.Helper = {};
    app.Helper.defaultErrorView = function(){
        return  {
            'targetClass': 'bfa-validator',
            'errorClass': 'bfa-validator-error',
            'errorHtml': '<div class="bfa-validator-error-mgs"> {{message}} </div>'
        };
    };

    //// Field helpers > Get All elements that has the data attribute
    app.Helper.getCloseParent = function(el, tag ){
        var classList, hasClass, finder;
        finder = el;
        while(finder.parentNode)
            {
                finder = finder.parentNode;
                if(tag.type === "class"){
                    if(finder.className){
                        classList =  finder.className.split(" ");
                        hasClass = false;
                        for (var i = 0; i < classList.length; i++) {
                            if(classList[i] === tag.value) {
                                hasClass = true;
                                break;
                            }
                        }
                        if (hasClass) break;
                    }
                }else{
                    if (finder.tagName === tag.value) break;
                }
            }
        if(finder === window.document) finder = null;
        return finder;
    };

    app.Fields.getElements = function(){
        var elems, rule, ruleClass;
        //Search for all avaliable rules
        for (var i = 0; i < app.Rule.library.length; i++) {
            //Get Class
            rule = app.Rule.library[i];
            ruleClass = rule['class'];
            //Get Elements
            elements = document.getElementsByClassName( ruleClass );
            if(elements){
                //Loop all elements
                for (var a = 0; a < elements.length; a++) {
                    //Get Form parent
                    var formParent = app.Helper.getCloseParent(elements[a], {'value':'FORM', 'type': 'element'});
                    //Populate
                    app.Fields.library.push({ 'rule': ruleClass, 'el': elements[a], 'form': formParent});
                }
            }
        }
    };

    //// Fields Methods

    ///// Validate > run rule on field
    app.Fields.Validate = function(field, customRule){
        var rule = app.Rule.get(field.rule);
        var test = { 'valid': true};
        //Rule not found -- pass
        if(!rule) {
            test.valid = false;
            test.errorMessage = 'Error: Rule not found';
            return test;
        }
        //Validate rule
        if(!rule.validation.test(field.el.value)){
            test.valid = false;
            test.errorMessage = rule.errorMessage[scope.options.lang];
        }
        return test;
    };

    app.Fields.ShowError = function(field, validate){
        if(field.error && field.showError) return;
        field.showError = true;
        //Get view
        var view = scope.options.errorView;
        var errorPlace;

        if(view.targetClass){
            //Go to targe
            errorPlace = app.Helper.getCloseParent(field.el, {'value':view.targetClass, 'type': 'class'});
        }

        if(errorPlace){
            // field.error = true;
            var errorDiv = document.createElement("div");
            errorDiv.className = view.errorClass + ' ' + view.errorClass + '-' + field.rule;
            innerError = view.errorHtml.replace(new RegExp('{{message}}', 'g'), validate.errorMessage);
            errorDiv.innerHTML = innerError;
            field.errorDiv = errorPlace.appendChild(errorDiv);
        }
    };

    app.Fields.HideError = function(field, validate){
        if(!field.error && !field.showError) return;
        field.showError = false;
        //Get view
        var view = scope.options.errorView;
        var errorPlace, errorClass, errorDiv;

        if(view.targetClass){
            //Go to targe
            errorPlace = app.Helper.getCloseParent(field.el, {'value':view.targetClass, 'type': 'class'});
        }

        if(errorPlace){
            field.errorDiv.parentNode.removeChild(field.errorDiv);
        }
    };

    //// Fields Handlers

   //////
    app.Fields.Handlers = function(){
        //Events - onkeyup, onblur, onchange, onsubmit

        // var fieldEvents = [ 'keyup', 'blur', 'change'];
        var formEvents = [ 'submit'];

        //Add event
        var addHandler = function(el, evt, callback){
            if (el.addEventListener){
                el.addEventListener( evt, callback, false);
            }else if(el.attachEvent){
                el.attachEvent("on"+evt, callback);

            }
        };

        //Group validates by form
        var formHandler = function(form, field){
            //Verify with form is already added
            var formAdded = false;
            var formData;
            for (var i = 0; i < app.Fields.forms.length; i++) {
                if(form === app.Fields.forms[i].el){
                    formData = app.Fields.forms[i];
                    formAdded = true;
                    break;
                }
            };
            //If it is add filed to array of tests
            var formData;
            if(formAdded){
                app.Fields.forms[i].validations.push(field);
            }else{
                formData = { 'el': form, 'validations': [field]};
                app.Fields.forms.push( formData );
            }

            // If it isn't add first handler
            if(formAdded) return;
            addHandler(formData.el, formEvents[0], function (e) {

                    var validate, field, errorForm;

                    for (var i = 0; i < this.validations.length; i++) {
                        field = this.validations[i];
                        validate = app.Fields.Validate(field);
                        field.error = !validate.valid;

                        if(!errorForm && field.error){
                            errorForm = true;
                        }

                       if(field.error){
                            app.Fields.ShowError(field, validate);
                       }else{
                            app.Fields.HideError(field, validate);
                       }
                    }

                    if(errorForm){
                        if(e.preventDefault){
                            e.preventDefault();
                        }else{
                            e.returnValue = false;
                        }
                    }

                }.bind(formData));
        };

        var field, parentForm;
        for (var i = 0; i < app.Fields.library.length; i++) {
            field = app.Fields.library[i];
            parentForm = field.form;
            if(parentForm) {
                formHandler(parentForm, field);
            }
        }
    };



    ieFixes();
    //
    return scope;

}(BFA.Validator));

